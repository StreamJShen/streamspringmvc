package com.streamMVC.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.AsyncHandlerInterceptor;

import com.streamMVC.exception.AuthorizationException;

public class LoginLnterceptor implements AsyncHandlerInterceptor{
	private List<String> excludedUrls;

    public void setExcludedUrls(List<String> excludedUrls) {
        this.excludedUrls = excludedUrls;
    }
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("3");
		System.out.println("LoginLnterceptor->preHandle");
        String requestUri = request.getRequestURI();
        for (String url : excludedUrls) {
            if (requestUri.startsWith(url)) {
            	System.out.println(true);
                return true;
            }
        }
//        if(requestUri.endsWith(".css"))
//        {
//        	return true;
//        }
        
        // intercept
        
        System.out.println(requestUri);
//        System.out.println(false);
//        request.getRequestDispatcher("/Login").forward(request, response);
//        HttpSession session = request.getSession();
//        if (session.getAttribute("user") == null) {
////            throw new AuthorizationException();
//        	System.out.println("nosession");
//        	request.getRequestDispatcher("/Login").forward(request, response);
//            return true;
//        } else {
            return true;
//        }
    }
}
