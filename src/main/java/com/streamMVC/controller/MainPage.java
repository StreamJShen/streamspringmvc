package com.streamMVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author sshen109
 *	Login Page
 */
@Controller
public class MainPage {
	
	public MainPage() {
        System.out.println("----MainPage.java 的 构造器-------");
    }
	
	@RequestMapping("/Main")
	public String login(){
		System.out.println("MainPage->Main");
		return "main";
	}

}

