package com.streamMVC.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author sshen109
 *	Login Page
 */
@Controller
public class Login {
	
	public Login() {
        System.out.println("----Login.java 的 构造器-------");
    }
	
	@RequestMapping("/Login")
	public String login(HttpServletRequest request,String username,String password){
//		HttpSession session = request.getSession();
//		System.out.println("New? "+session.isNew());
//		System.out.println("session ID:"+session.getId());
//		System.out.println("session Time:"+new Date(session.getCreationTime()));
//		System.out.println("try login with "+username+"/"+password);
//		System.out.println("session: "+session.getAttribute("user"));
//		request.getSession().setAttribute("user", username); 
//		session.setAttribute("user", username); 
//		System.out.println("session: "+session.getAttribute("user"));
		return "login";
	}
}

