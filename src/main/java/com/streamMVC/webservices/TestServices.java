package com.streamMVC.webservices;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.streamMVC.DTO.TestDTO;


@Controller
@RequestMapping(value = "/webservices")
public class TestServices {
	
	@RequestMapping(value = "/test")
	@ResponseBody
	public TestDTO testWebservice(@RequestBody final String testDTO){
		System.out.println("TestServices->testWebservice");
		return new TestDTO();
	}
}
