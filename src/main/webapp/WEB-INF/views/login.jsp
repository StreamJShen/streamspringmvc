<html lang="en">

<head>
<%@ page session="true"%>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="webjars/bootstrap/4.0.0-2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="resources/css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
SessionID:<%=session.getId()%><br/><br/>
  IsNew:<%=session.isNew() %><br/><br/>
  MaxInactiveInternal:<%=session.getMaxInactiveInterval()%><br/><br/>
  CreateTime:<%=session.getCreationTime() %><br/><br/>
  LastAccessTime:<%=session.getLastAccessedTime() %><br/><br/>
    <form class="form-signin" action="Login" method="post">
      <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <!-- <input type="email" id="inputEmail" name="username" class="form-control" placeholder="Email address" required="" autofocus=""> -->
      <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Email address" required="" autofocus="">
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">© 2017-2018</p>
    </form>
  

</body></html>